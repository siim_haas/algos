
public class Balls {

	enum Color {
		green, red
	};

	static Color[] balls = { Color.green, Color.green, Color.red, Color.red, Color.green, Color.red, Color.red,
			Color.red, Color.green, Color.red };
	// static int[] counter = { 0, 0 };

	public static void main(String[] param) {

		for (int i = 0; i < balls.length; i++) {
			System.out.print(balls[i].toString() + " ");
		}
		System.out.println();
		reorder(balls);
		for (int i = 0; i < balls.length; i++) {
			System.out.print(balls[i].toString() + " ");
		}
	}

	public static void reorder(Color[] balls) {
		Color[] buffer = new Color[balls.length];
		int[] counter = { 0, 0 };
				
		for (int i = 0; i < balls.length; i++) {
			if (balls[i] == Color.green) {
				counter[1]++;
			} else {
				counter[0]++;
			}
		}
		for (int i = 0; i < counter.length - 1; i++) {
			counter[i + 1] = counter[i + 1] + counter[i];
		}
		for (int i = balls.length - 1; i >= 0; i--) {
			if (balls[i] == Color.green) {
				buffer[counter[1] - 1] = balls[i];
				counter[1]--;
			} else {
				buffer[counter[0] - 1] = balls[i];
				counter[0]--;
			}
		}
		for (int i = 0; i < buffer.length; i++) {
			balls[i] = buffer[i];
		}
	}
}
